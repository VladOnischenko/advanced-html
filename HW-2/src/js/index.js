const burger = document.querySelector('.header__burger');
const menu = document.querySelector('.menu');
const body = document.querySelector('body');

burger.addEventListener('click', (event) =>{
	event.currentTarget.classList.toggle('active');
	menu.classList.toggle('active');
	body.classList.toggle('lock');
})